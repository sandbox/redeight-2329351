<?php
function _webform_defaults_vip_date() {
  $default_date = date("Y-m-d H:i");
  $repeat = array(
    'show_repeat_settings' => FALSE,
    'rrule' => ''
  );
  $dates = array(
    'date' => $default_date,
    'repeat' => $repeat,
  );
  $defaults = array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 1,
    'extra' => array(
      'datetime' => $dates,
      'seats_total' => 20,
      'seats_max_per_reg' => 4,
      'date_display_limit' => '+2 weeks +1 day 0:00:00',
      'close_registration' => '+1 hours',
      'private' => 0,
    ),
  );

  return $defaults;
}

function _webform_theme_vip_date() {
  $theme = array(
    'webform_display_vip_date' => array(
      'render element' => 'element',
    ),
  );

  return $theme;
}

function _webform_edit_vip_date($component) {
  $form = array();
  // Include the date
  $default_date = isset($component['extra']['datetime']['date']) ? $component['extra']['datetime']['date'] : date("Y-m-d H:i:s");
  $format = 'Y-m-d H:i:s';
  $form['extra']['datetime']['date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date'),
    '#tree' => TRUE,
    '#default_value' => $default_date,
    '#date_format' => $format,
    //'#date_timezone' => 'America/Los_Angeles',
    '#date_timezone' => date_default_timezone(),
    '#date_label_position' => 'within', // See other available attributes and what they do in date_api_elements.inc
    '#date_increment' => 15, // Optional, used by the date_select and date_popup elements to increment minutes and seconds.
    '#date_year_range' => '-3:+3', // Optional, used to set the year range (back 3 years and forward 3 years is the default).
    '#datepicker_options' => array(),
  );
  $form['extra']['datetime']['repeat'] = array(
    '#tree' => TRUE,
  );
  $form['extra']['datetime']['repeat']['show_repeat_settings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Repeat'),
    '#default_value' => isset($component['extra']['datetime']['repeat']['show_repeat_settings']) ? $component['extra']['datetime']['repeat']['show_repeat_settings'] : FALSE,
    '#weight' => 1,
  );
  $form['extra']['datetime']['repeat']['rrule'] = array(
    '#type' => 'date_repeat_rrule',
    '#theme_wrappers' => array('date_repeat_rrule'),
    '#default_value' => isset($component['extra']['datetime']['repeat']['rrule']) ? $component['extra']['datetime']['repeat']['rrule'] : '',
    '#date_timezone' => date_default_timezone(),
    '#date_label_position' => 'above',
    '#date_format' => variable_get('date_format_short', 'Y-m-d H:i:s'),
    '#weight' => 2,
    '#date_year_range' => '-0:+7',
    '#date_repeat_collapsed' => FALSE,
  );
  $form['extra']['date_display_limit'] = array(
    '#title' => t('Repeat display limit'),
    '#description' => t('Sets a limit on how far out dates will be displayed. See <a href="http://php.net/manual/en/function.strtotime.php">PHP strtotime function</a> for info and <a href="https://www.functions-online.com/strtotime.html">strtotime test</a> to test.'),
    '#type' => 'textfield',
    '#default_value' => isset($component['extra']['date_display_limit']) ? $component['extra']['date_display_limit'] : '+2 weeks +1 day 0:00:00',
    '#size' => 60,
    '#required' => FALSE,
    '#weight' => 3,
    '#element_validate' => array('_strtotime_limit_validate'),
  );
  $form['extra']['close_registration'] = array(
    '#title' => t('Close registration'),
    '#description' => t('Close registration relative to the event start. See <a href="http://php.net/manual/en/function.strtotime.php">PHP strtotime function</a> for info and <a href="https://www.functions-online.com/strtotime.html">strtotime test</a> to test.'),
    '#type' => 'textfield',
    '#default_value' => isset($component['extra']['close_registration']) ? $component['extra']['close_registration'] : '+1 hours',
    '#size' => 60,
    '#required' => FALSE,
    '#weight' => 3,
    '#element_validate' => array('_strtotime_close_validate'),
  );
  $form['extra']['seats_total'] = array(
    '#title' => t('Seats total'),
    '#description' => t('The maximum number of slots which can be registered at one time.'),
    '#type' => 'textfield',
    '#default_value' => isset($component['extra']['seats_total']) ? $component['extra']['seats_total'] : 4,
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#weight' => 4, 
  );
  $form['extra']['seats_max_per_reg'] = array(
    '#title' => t('Max reservations'),
    '#description' => t('The maximum number of slots which can be registered at one time.'),
    '#type' => 'textfield',
    '#default_value' => isset($component['extra']['seats_max_per_reg']) ? $component['extra']['seats_max_per_reg'] : 4,
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#weight' => 5, 
  );
  
  return $form;
}

/**
 * Render the component in the webform itself. (FRONTEND)
 */
function _webform_render_vip_date($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $date_options = _get_vip_dates($component);
  if (count($date_options) == 0) {
    $date_options = array('none' => 'There are no dates available at this time. Please check back later.');
  }
  $vip_date = array(
    '#title' => t('Date'),
    '#type' => 'select',
    '#default_value' => $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'],
    '#options' => $date_options,
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    //'#theme_wrappers' => array('webform_element'),
    //'#webform_component' => $component['type'],
    '#element_validate' => array('_vip_date_date_validate'),
  );

  $seat_options = array();
  for ($i = 1; $i <= min($component['extra']['seats_max_per_reg'], $component['extra']['seats_total']); $i++){
    $seat_options[$i] = $i;
  }
  $adults = array(
    '#title' => t('Tickets'),
    '#description' => t('!desc Max: !max', 
                        array('!desc' => $component['extra']['description'], '!max' =>  min($component['extra']['seats_max_per_reg'], $component['extra']['seats_total']), )
                      ),
    '#type' => 'select',
    '#default_value' => $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'],
    '#options' => $seat_options,
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'] + 1, 
    '#element_validate' => array('_vip_date_adults_validate'),
  );

  $elements = array(
    'vip_date' => $vip_date,
    'adults' => $adults, 
  );

  return $elements;
}

function _webform_display_vip_date($component, $value, $format = 'html') {
  $vip_date = array(
    '#title' => 'Date',
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_vip_date',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#parents' => array(NULL),
    '#value' => $value['vip_date'],
  );

  $adults = array(
    '#title' => t('Adults'),
    '#type' => 'item',
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_vip_date',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#parents' => array(NULL),
    '#value' => $value['adults'],
  );

  $elements = array(
    'vip_date' => $vip_date,
    'adults' => $adults,
  );

  return $elements;
}

function theme_webform_display_vip_date($variables) {
  $element = $variables['element'];
  $value = $element['#value'];
  if ($element['#title'] == t('VIP Date')) {
    return _format_vip_date($value, 'YmdHi', 'D M jS Y \a\t h:i A');
  }
  if ($element['#title'] == t('Adults')) {
    return check_plain($value);
  }
  return trim($value) !== '' ? $value : ' ';
}

function _webform_table_vip_date($component, $value) {
  $date = _format_vip_date($value['vip_date'], 'YmdHi', 'M jS Y h:i A');
  $adults = $value['adults'];
  $output = array(
    'VIP Date' => $date, 
    'Adults' => $adults,
  );
  return $output;
}

function _webform_analysis_vip_date($component, $sids = array(), $single = FALSE) {
  $header = array(
    'date' => t('Date'), 
    'count' => t('Adults'),
  );
  $dates = _get_vip_dates_count($component['nid']);

  $rows = array();
  foreach ($dates as $date => $count) {
    $row = array(
      'date' => _format_vip_date($date, 'YmdHi',  'M jS Y h:i A'),
      'count' => $count,
    );
    $rows[] = $row;
  }

  $other[] = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('webform-grid'))));

  return array(
    'table_rows' => array(),
    'other_data' => $other,
  );
  /*// Generate the list of options and questions.
  $options = _webform_select_options_from_text($component['extra']['options'], TRUE);
  $questions = _webform_select_options_from_text($component['extra']['questions'], TRUE);

  // Generate a lookup table of results.
  $query = db_select('webform_submitted_data', 'wsd')
    ->fields('wsd', array('no', 'data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid'])
    ->condition('data', '', '<>')
    ->groupBy('no')
    ->groupBy('data');
  $query->addExpression('COUNT(sid)', 'datacount');

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $result = $query->execute();
  $counts = array();
  foreach ($result as $data) {
    $counts[$data->no][$data->data] = $data->datacount;
  }

  // Create an entire table to be put into the returned row.
  $rows = array();
  $header = array('');

  // Add options as a header row.
  foreach ($options as $option) {
    $header[] = $option;
  }

  // Add questions as each row.
  foreach ($questions as $qkey => $question) {
    $row = array($question);
    foreach ($options as $okey => $option) {
      $row[] = !empty($counts[$qkey][$okey]) ? $counts[$qkey][$okey] : 0;
    }
    $rows[] = $row;
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('webform-grid'))));

  return array(array(array('data' => $output, 'colspan' => 2)));*/
}

function _vip_date_date_validate($element, &$form_state){
  // Check for non-values
  if($element['#value'] == NULL || $element['#value'] == '' || $element['#value'] == 'none') {
    form_set_error(implode('][', $element['#parents']), t('There appear to be no available dates. Please check back later.'));
  }
}

function _vip_date_adults_validate($element, &$form_state){
  foreach ($form_state['webform']['component_tree']['children'] as $child) {
    if ($child['type'] ==  'vip_date') {
      $component = $child;
    }
  }
  if($element['#value'] < 1) {
    form_set_error(implode('][', $element['#parents']), t('You must select a positive value.'));
  }
  if(!is_numeric($element['#value'])) {
    form_set_error(implode('][', $element['#parents']), t('Adults must be a numerical value.'));
  }
  if(isset($component)) {
    $date = $form_state['values']['submitted'][$component['form_key']]['vip_date'];
    $seats_taken = _get_vip_dates_count($component['nid']);
    $seats_total = $component['extra']['seats_total'];
    $seats_available = $seats_total - (isset($seats_taken[$date]) ? $seats_taken[$date] : 0);
    if($element['#value'] > $seats_available) {
      form_set_error(implode('][', $element['#parents']), t('There !switch only %seats_available seats available at %date_nice', array('!switch' => $seats_available > 1 ? 'are' : 'is', '%seats_available' => $seats_available, '%date_nice' => _format_vip_date($date, 'YmdHi', 'h:i A \o\n M jS Y'))));
    }
  }
}

function _strtotime_limit_validate($element, &$form_state){
  if ($element['#value'] != '') {
    $formatted_date_limit = date('YmdHi', strtotime($element['#value']));
    if (!$formatted_date_limit) {
      form_set_error(implode('][', $element['#parents']), t('%date is not a valid strtotime format and was unable to parse correctly.', array('%date' => $element['#value'])));
    }
    if ($formatted_date_limit <= date('YmdHi')) {
      form_set_error(implode('][', $element['#parents']), t('Your limiting date must be after the current day. Current Date: %current_date. Parsed Date: %date.', array('%date' => _format_vip_date($formatted_date_limit, 'YmdHi', 'h:i:s a, M jS, Y'), '%current_date' => date('h:i:s a, M jS, Y'))));
    }
  }
}

function _strtotime_close_validate($element, &$form_state){
  if ($element['#value'] != '') {
    $formatted_date_limit = date('YmdHi', strtotime($element['#value']));
    if (!$formatted_date_limit) {
      form_set_error(implode('][', $element['#parents']), t('%date is not a valid strtotime format and was unable to parse correctly.', array('%date' => $element['#value'])));
    }
  }
}

/**
 * Returns array of available VIP dates for the current node.
 */
function _get_vip_dates($component){
  //Construct the "fields" for 'date_repeat_build_dates'
  $datefield = $component['extra']['datetime'];
  $datefield['settings'] = array(
    'granularity' => drupal_map_assoc(array('year', 'month', 'day', 'hour', 'minute')),
    'tz_handling' => 'site',
    'timezone_db' => 'UTC',
  );
  $datefield['type'] = 'datetime';
  $datefield['field_name'] = 'vip_date';
  $start = $component['extra']['datetime']['date'];
  $item = array(
    'value' => $start,
    'timezone' => 'America/Los_Angeles',
  );
  $repeat = $component['extra']['datetime']['repeat']['show_repeat_settings'];
  $rrule = $component['extra']['datetime']['repeat']['rrule'];
  if ($repeat) {
    $dates = date_repeat_build_dates($rrule, NULL, $datefield, $item);
  } else {
    $dates = array(array('value' => $start));
  }

  $options = array();

  $max_registrations = $component['extra']['seats_total'];
  $current_date = date('YmdHi');
  if ($component['extra']['date_display_limit'] == '' || $component['extra']['date_display_limit'] == 'none') {
    $date_display_limit = date('YmdHi', strtotime('+10 years'));
  } else {
    $date_display_limit = date('YmdHi', strtotime($component['extra']['date_display_limit']));
  }
  if ($component['extra']['close_registration'] == '' || $component['extra']['close_registration'] == 'none') {
    $close_registration = $current_date;
  } else {
    $close_registration = date('YmdHi', strtotime($component['extra']['close_registration']));
  }
  $registration_counts = _get_vip_dates_count($component['nid']);
  foreach ($dates as $date) {
    $count = $max_registrations;
    $stored_date = _format_vip_date($date['value'], 'Y-m-d H:i:s', 'YmdHi');
    // drop dates past the date limit
    if ($stored_date > $date_display_limit) {
      // since dates is ordered by date, we can stop
      break;
    }
    // skip dates that should no longer be available. 
    if ($stored_date < $close_registration) {
      // we'll skip this one
      continue;
    }
    // skip dates that are full
    if (isset($registration_counts[$stored_date])) {
      if ($registration_counts[$stored_date] >= $max_registrations) {
        continue;
      }
      $count = $max_registrations - $registration_counts[$stored_date];
    }
    $displayed_date = _format_vip_date($date['value'], 'Y-m-d H:i:s', 'D h:i A, M jS Y');
    $options[$stored_date] = $displayed_date . ' (Available: ' . $count . ')';
  }

  return $options;
}

/**
 * Helper function that converts dates between formats.  
 */
function _format_vip_date($datestring, $original_format, $target_format){
  return date_format(date_create_from_format($original_format, $datestring), $target_format);
}

/**
 * Helper function to get current attendance count for a nid
 */
function _get_vip_dates_count($nid){
  $query = db_select('webform_vip_registration', 'v')
    ->fields('v', array('date',))
    ->condition('v.nid', $nid)
    ->groupBy('date');
  $query->addExpression('SUM(num)', 'count');
  $dates = $query->execute()
    ->fetchAllKeyed(0, 1); // creates an array in the form of 'date' => 'count'
  return $dates;
}
